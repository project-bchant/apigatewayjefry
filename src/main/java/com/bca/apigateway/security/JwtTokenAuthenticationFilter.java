package com.bca.apigateway.security;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Hex;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

public class JwtTokenAuthenticationFilter extends  OncePerRequestFilter {
	
	private final JwtConfig jwtConfig;
	
	public JwtTokenAuthenticationFilter(JwtConfig jwtConfig) {
		this.jwtConfig = jwtConfig;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
		RequestWrapper RequestWrapper = new RequestWrapper((HttpServletRequest) request);
		//Step 1 check header validation
		String header = RequestWrapper.getHeader("Authorization");
		String xonce = RequestWrapper.getHeader("xonce");
		String digest = RequestWrapper.getHeader("xdigest");
		String uris = RequestWrapper.getScheme();
		logger.info("In Internal Filter, Client IP: "+RequestWrapper.getRemoteAddr());
		logger.info(uris + " " + RequestWrapper.isSecure() + " "+ request.getRequestURI());
		if(header == null || !header.startsWith(jwtConfig.getPrefix())
				|| xonce == null || digest == null
				) {
			logger.error("Request Header not valid");
			logger.info(header+" "+xonce+" "+digest);
			chain.doFilter(RequestWrapper, response);
			return;
		}
		
		//Step 2 check jwt validation
		String token = header.replace(jwtConfig.getPrefix(),"");
		Claims claims = null;
		try {
			claims = Jwts.parser().setSigningKey(jwtConfig.getSecret()).parseClaimsJws(token).getBody();
			logger.info("Token valid");
		}catch (SignatureException e) {
            logger.error("Invalid JWT signature");
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT Token.");
        } catch (ExpiredJwtException e) {
            logger.error("Expired JWT token.");
        } catch (UnsupportedJwtException e) {
            logger.error("Unsupported JWT token.");
        } catch (IllegalArgumentException e) {
            logger.error("JWT token compact of handler are invalid.");
        }
		if(claims == null) {
			chain.doFilter(RequestWrapper, response);
			return;
		}
		
		//Step 3 check digest
		String method = RequestWrapper.getMethod();
		String uri = "https://bchant.xyz"+RequestWrapper.getRequestURI();
		String body = sha256(RequestWrapper.getBody());
		String gethash = getHash(method, uri, token,body,xonce);
		Long diff = compareTimeServer(xonce);

		System.out.println("xdigest: "+digest);
		System.out.println("SHA body: "+ body + " " + RequestWrapper.getBody());
		System.out.println("Diff server-client: "+ diff);
		System.out.println("Gethash: "+gethash);
		
		if(digest.equalsIgnoreCase(gethash) && diff >= -120L && diff <= 6000L) {
			logger.info("Digest Valid creating token");
			String username = claims.getSubject();
			if(username != null) {
				@SuppressWarnings("unchecked")
				List<String> authorities = (List<String>) claims.get("authorities");
				UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
								 username, null, authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
				SecurityContextHolder.getContext().setAuthentication(auth);
				String tokens = generateToken(claims);
				response.addHeader(jwtConfig.getHeader(), jwtConfig.getPrefix() + tokens);
			}
		}
		else {
			logger.info("Unauthorized Request");
			SecurityContextHolder.clearContext();
		}
		chain.doFilter(RequestWrapper, response);
	}
	
    public String generateToken(Claims claims) {
		Long now = System.currentTimeMillis();
		logger.info("Generate token");
		return Jwts.builder()
			.setSubject(claims.getSubject())	
			.claim("authorities", claims.get("authorities"))
			.setIssuedAt(new Date(now))
			.setExpiration(new Date(now + jwtConfig.getExpiration() * 1000))
			.signWith(SignatureAlgorithm.HS512, jwtConfig.getSecret())
			.compact();
    }
    
    private String sha256(String input) {
    	try {
	        MessageDigest md = MessageDigest.getInstance("SHA-256");  
	        byte[] messageDigest = md.digest(input.getBytes(StandardCharsets.UTF_8)); 
	        StringBuffer hexString = new StringBuffer();
	        for (int i = 0; i < messageDigest.length; i++) {
	        String hex = Integer.toHexString(0xff & messageDigest[i]);
	        if(hex.length() == 1) hexString.append('0');
	            hexString.append(hex);
	        }
	        return hexString.toString(); 
		} catch (Exception e) {
			logger.error("Fail Hash");
			return "";
		}
    }
	
    private Long compareTimeServer(String xonce)  {
    	long diff = -300L;
    	try {
    		TimeZone tz = TimeZone.getTimeZone("GMT");
	    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	    	df.setTimeZone(tz);
	    	String server = df.format(new java.util.Date());
	    	System.out.println("serverdate: "+server);
			java.util.Date serverdate = df.parse(server);
			java.util.Date clientdate = df.parse(xonce);
			diff = (serverdate.getTime() - clientdate.getTime())/1000;
		} catch (ParseException e) {
			logger.error("Error parse time");
		}
    	return diff;
    }
    
    public String getHash(String method ,String url ,String token, String body ,String xonce) {
    	try {
            String secret = "asdf";
    		String stringtosign = method+url+token+body+xonce;
    		System.out.println("stringtosign : "+stringtosign);
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            return Hex.encodeHexString(sha256_HMAC.doFinal(stringtosign.getBytes("UTF-8")));
           }
        catch (Exception e){
        	logger.error("Error getHash");
        	return "";
        }
    }
}