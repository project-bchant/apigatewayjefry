package com.bca.apigateway;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class StaticContentController implements ErrorController {

	@RequestMapping(value = "/**/{[path:[^\\.]*}")
	public String index(final HttpServletRequest request) {
	    return "forward:/index.html";
	}
	
	@RequestMapping(value = "/error")
	public String error(final HttpServletRequest request) {
	    return "forward:/404";
	}
	
    @Override
    public String getErrorPath() {
        return "/error";
    }
}